'use strict';

/**
 * qlkh-tin-tuc service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::qlkh-tin-tuc.qlkh-tin-tuc');
