'use strict';

/**
 * qlkh-tin-tuc router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::qlkh-tin-tuc.qlkh-tin-tuc');
