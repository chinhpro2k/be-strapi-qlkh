'use strict';

/**
 * qlkh-tin-tuc controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::qlkh-tin-tuc.qlkh-tin-tuc');
