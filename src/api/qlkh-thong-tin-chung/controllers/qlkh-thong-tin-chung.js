'use strict';

/**
 * qlkh-thong-tin-chung controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::qlkh-thong-tin-chung.qlkh-thong-tin-chung');
