'use strict';

/**
 * qlkh-thong-tin-chung router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::qlkh-thong-tin-chung.qlkh-thong-tin-chung');
