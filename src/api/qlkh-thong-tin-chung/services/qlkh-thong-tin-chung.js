'use strict';

/**
 * qlkh-thong-tin-chung service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::qlkh-thong-tin-chung.qlkh-thong-tin-chung');
