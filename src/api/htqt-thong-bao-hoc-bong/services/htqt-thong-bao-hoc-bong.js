'use strict';

/**
 * htqt-thong-bao-hoc-bong service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-thong-bao-hoc-bong.htqt-thong-bao-hoc-bong');
