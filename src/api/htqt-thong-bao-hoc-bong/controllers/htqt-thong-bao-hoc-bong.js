'use strict';

/**
 * htqt-thong-bao-hoc-bong controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-thong-bao-hoc-bong.htqt-thong-bao-hoc-bong');
