'use strict';

/**
 * htqt-thong-bao-hoc-bong router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-thong-bao-hoc-bong.htqt-thong-bao-hoc-bong');
