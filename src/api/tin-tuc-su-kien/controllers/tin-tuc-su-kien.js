'use strict';

/**
 * A set of functions called "actions" for `tin-tuc-su-kien`
 */

module.exports = {
  getAllData: async (ctx, next) => {
    try {
    
      const data = await strapi
        .service("api::tin-tuc-su-kien.tin-tuc-su-kien")
        .articleApiCustom(ctx);
      ctx.body = data;
    } catch (err) {
      ctx.badRequest("controller error", { moreDetails: err });
    }
  }
};
