'use strict';

/**
 * tin-tuc-su-kien service
 */

module.exports = () => ({
  articleApiCustom: async (ctx) => {
    try {
      const { query } = ctx;

      const [entries, count] = await strapi.db.query('api::qlkh-tin-tuc.qlkh-tin-tuc').findWithCount({
        // select: ['title', 'description'],
        where: { kieu: query.type ? { $eq: query.type } : {}, tieuDe: query.tieuDeStr ? { $containsi: query.tieuDeStr } : {},publishedAt:{$notNull:true} },
        orderBy: [ { publishedAt: 'dsc' }],
        populate: ["deep"],
        offset: query?.page ? (+query?.limit*(+query?.page-1)) : 1,
        limit: query?.limit ? +query?.limit : 3
      });
      console.log('entries=====', entries);
      const dataReturn = entries?.map((item) => {
        const items = { ...item }
        delete items.hinhAnh
        return {

          ...items,
          imageUrl: item.hinhAnh?.formats?.thumbnail?.url


        }
      })

      return {
        data: dataReturn,
        metadata: {
          page: query?.page ?? 1,
          limit: query?.limit ?? 3,
          total: count
        }
      }
    } catch (err) {
      return err
    }
  }
});
