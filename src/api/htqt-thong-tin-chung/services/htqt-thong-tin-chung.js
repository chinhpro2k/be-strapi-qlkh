'use strict';

/**
 * htqt-thong-tin-chung service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-thong-tin-chung.htqt-thong-tin-chung');
