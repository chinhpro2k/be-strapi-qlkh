'use strict';

/**
 * htqt-thong-tin-chung router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-thong-tin-chung.htqt-thong-tin-chung');
