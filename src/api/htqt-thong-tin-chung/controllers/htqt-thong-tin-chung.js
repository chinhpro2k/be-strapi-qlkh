'use strict';

/**
 * htqt-thong-tin-chung controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-thong-tin-chung.htqt-thong-tin-chung');
