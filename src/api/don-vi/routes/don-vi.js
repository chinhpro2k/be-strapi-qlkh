module.exports = {
  routes: [
    {
     method: 'GET',
     path: '/don-vi/all',
     handler: 'don-vi.getAllData',
     config: {
       policies: [],
       middlewares: [],
     },
    },
  ],
};
