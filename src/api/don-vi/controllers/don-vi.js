'use strict';

/**
 * A set of functions called "actions" for `don-vi`
 */


module.exports = {
  getAllData: async (ctx, next) => {
    try {
    
      const data = await strapi
        .service("api::don-vi.don-vi")
        .articleApiCustom(ctx);
      ctx.body = data;
    } catch (err) {
      ctx.badRequest("controller error", { moreDetails: err });
    }
  }
};
