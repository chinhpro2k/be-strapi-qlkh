'use strict';

/**
 * don-vi service
 */

module.exports = () => ({
  articleApiCustom: async (ctx) => {
    try {
      const { query } = ctx;

      const [entries, count] = await strapi.db.query('api::don-vi-nghien-cuu.don-vi-nghien-cuu').findWithCount({
        // select: ['title', 'description'],
        where: { kieu: query.type ? { $eq: query.type } : {},publishedAt:{$notNull:true} },
        orderBy: [ { publishedAt: 'dsc' }],
        populate: ["deep"],
        offset: query?.page ? (+query?.limit*(+query?.page-1)) : 1,
        limit: query?.limit ? +query?.limit : 3
      });
      const dataReturn = entries?.map((item) => {
        const items = { ...item }
        delete items.hinhAnh
        return {

          ...items,
          imageUrl: item.hinhAnh?.formats?.thumbnail?.url


        }
      })

      return {
        data: dataReturn,
        metadata: {
          page: +query?.page ?? 1,
          limit: +query?.limit ?? 3,
          total: count
        }
      }

    } catch (err) {
      return err
    }
  }
});