'use strict';

/**
 * htqt-van-ban-quy-dinh router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-van-ban-quy-dinh.htqt-van-ban-quy-dinh');
