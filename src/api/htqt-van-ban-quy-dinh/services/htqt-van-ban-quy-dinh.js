'use strict';

/**
 * htqt-van-ban-quy-dinh service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-van-ban-quy-dinh.htqt-van-ban-quy-dinh');
