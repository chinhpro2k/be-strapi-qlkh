'use strict';

/**
 * htqt-van-ban-quy-dinh controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-van-ban-quy-dinh.htqt-van-ban-quy-dinh');
