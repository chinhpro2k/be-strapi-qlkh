'use strict';

/**
 * chien-luoc-phat-trien router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::chien-luoc-phat-trien.chien-luoc-phat-trien');
