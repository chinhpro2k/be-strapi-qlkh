'use strict';

/**
 * chien-luoc-phat-trien service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::chien-luoc-phat-trien.chien-luoc-phat-trien');
