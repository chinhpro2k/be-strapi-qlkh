'use strict';

/**
 * chien-luoc-phat-trien controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::chien-luoc-phat-trien.chien-luoc-phat-trien');
