'use strict';

/**
 * qlkh-dinh-huong-nghien-cuu router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::qlkh-dinh-huong-nghien-cuu.qlkh-dinh-huong-nghien-cuu');
