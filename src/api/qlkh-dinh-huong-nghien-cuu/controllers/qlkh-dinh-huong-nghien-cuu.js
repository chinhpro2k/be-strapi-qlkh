'use strict';

/**
 * qlkh-dinh-huong-nghien-cuu controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::qlkh-dinh-huong-nghien-cuu.qlkh-dinh-huong-nghien-cuu');
