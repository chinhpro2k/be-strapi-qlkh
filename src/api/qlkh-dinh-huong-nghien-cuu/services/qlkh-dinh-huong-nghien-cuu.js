'use strict';

/**
 * qlkh-dinh-huong-nghien-cuu service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::qlkh-dinh-huong-nghien-cuu.qlkh-dinh-huong-nghien-cuu');
