'use strict';

/**
 * htqt-gioi-thieu router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-gioi-thieu.htqt-gioi-thieu');
