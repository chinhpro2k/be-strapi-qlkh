'use strict';

/**
 * htqt-gioi-thieu controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-gioi-thieu.htqt-gioi-thieu');
