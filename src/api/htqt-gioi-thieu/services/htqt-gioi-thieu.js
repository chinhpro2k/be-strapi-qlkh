'use strict';

/**
 * htqt-gioi-thieu service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-gioi-thieu.htqt-gioi-thieu');
