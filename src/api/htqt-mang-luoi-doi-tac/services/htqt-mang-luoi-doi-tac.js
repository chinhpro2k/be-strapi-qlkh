'use strict';

/**
 * htqt-mang-luoi-doi-tac service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-mang-luoi-doi-tac.htqt-mang-luoi-doi-tac');
