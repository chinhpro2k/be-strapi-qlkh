'use strict';

/**
 * htqt-mang-luoi-doi-tac router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-mang-luoi-doi-tac.htqt-mang-luoi-doi-tac');
