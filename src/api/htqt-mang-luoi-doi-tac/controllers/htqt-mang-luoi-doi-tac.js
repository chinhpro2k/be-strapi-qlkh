'use strict';

/**
 * htqt-mang-luoi-doi-tac controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-mang-luoi-doi-tac.htqt-mang-luoi-doi-tac');
