'use strict';

/**
 * htqt-cau-truc-trang-web controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-cau-truc-trang-web.htqt-cau-truc-trang-web');
