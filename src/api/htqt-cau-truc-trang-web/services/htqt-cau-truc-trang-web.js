'use strict';

/**
 * htqt-cau-truc-trang-web service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-cau-truc-trang-web.htqt-cau-truc-trang-web');
