'use strict';

/**
 * htqt-cau-truc-trang-web router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-cau-truc-trang-web.htqt-cau-truc-trang-web');
