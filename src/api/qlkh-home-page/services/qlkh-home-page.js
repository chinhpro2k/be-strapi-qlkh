'use strict';

/**
 * qlkh-home-page service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::qlkh-home-page.qlkh-home-page');
