'use strict';

/**
 * qlkh-home-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::qlkh-home-page.qlkh-home-page');
