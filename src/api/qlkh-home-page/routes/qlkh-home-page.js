'use strict';

/**
 * qlkh-home-page router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::qlkh-home-page.qlkh-home-page');
