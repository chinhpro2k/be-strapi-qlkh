'use strict';

/**
 * qlkh-cau-truc-trang-web controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::qlkh-cau-truc-trang-web.qlkh-cau-truc-trang-web');
