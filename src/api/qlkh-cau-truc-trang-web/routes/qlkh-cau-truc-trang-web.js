'use strict';

/**
 * qlkh-cau-truc-trang-web router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::qlkh-cau-truc-trang-web.qlkh-cau-truc-trang-web');
