'use strict';

/**
 * qlkh-cau-truc-trang-web service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::qlkh-cau-truc-trang-web.qlkh-cau-truc-trang-web');
