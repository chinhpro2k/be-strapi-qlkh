'use strict';

/**
 * van-ban-quy-dinh service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::van-ban-quy-dinh.van-ban-quy-dinh');
