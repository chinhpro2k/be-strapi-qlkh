'use strict';

/**
 * van-ban-quy-dinh controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::van-ban-quy-dinh.van-ban-quy-dinh');
