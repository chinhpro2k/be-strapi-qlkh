'use strict';

/**
 * van-ban-quy-dinh router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::van-ban-quy-dinh.van-ban-quy-dinh');
