'use strict';

/**
 * thong-tin-chung service
 */

module.exports = () => ({
    getThongTin: async (ctx) => {
        try {
            const { query } = ctx;
            const entries = await strapi.entityService.findMany("api::qlkh-thong-tin-chung.qlkh-thong-tin-chung",
                {
                    //   fields: ["slug", "title", "description"],
                    populate: ["deep"],
                    //   filters: {
                    //     // ...query,
                    //     kieu:ctx.query.kieu? { $eq: query.kieu }:{},
                    //     tieuDe:query.tieuDeStr? { $containsi: query.tieuDeStr }:{}
                    //   }
                })
            const value = { ...entries }
            const dataReturn = {
                ...value,
                logoHeader: entries?.logoHeader?.url,
                logoFooter: entries?.logoFooter?.url,
            }

            return dataReturn
        } catch (err) {
            return err
        }
    },
});
