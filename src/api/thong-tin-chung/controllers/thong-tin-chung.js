'use strict';

/**
 * A set of functions called "actions" for `thong-tin-chung`
 */

module.exports = {
  getAllData: async (ctx, next) => {
    try {
    
      const data = await strapi
        .service("api::thong-tin-chung.thong-tin-chung")
        .getThongTin(ctx);
      ctx.body = data;
    } catch (err) {
      ctx.badRequest("controller error", { moreDetails: err });
    }
  }
};
