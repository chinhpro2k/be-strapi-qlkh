'use strict';

/**
 * A set of functions called "actions" for `van-ban`
 */

module.exports = {
  getAllData: async (ctx, next) => {
    try {
    
      const data = await strapi
        .service("api::van-ban.van-ban")
        .articleApiCustom(ctx);
      ctx.body = data;
    } catch (err) {
      ctx.badRequest("controller error", { moreDetails: err });
    }
  }
};
