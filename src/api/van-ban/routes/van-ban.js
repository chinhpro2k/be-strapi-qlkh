module.exports = {
  routes: [
    {
     method: 'GET',
     path: '/van-ban/all',
     handler: 'van-ban.getAllData',
     config: {
       policies: [],
       middlewares: [],
     },
    },
  ],
};
