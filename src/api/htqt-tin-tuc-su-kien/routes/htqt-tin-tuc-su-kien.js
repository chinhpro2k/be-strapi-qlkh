'use strict';

/**
 * htqt-tin-tuc-su-kien router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-tin-tuc-su-kien.htqt-tin-tuc-su-kien');
