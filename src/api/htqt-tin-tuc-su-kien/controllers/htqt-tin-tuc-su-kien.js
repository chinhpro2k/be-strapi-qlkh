'use strict';

/**
 * htqt-tin-tuc-su-kien controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-tin-tuc-su-kien.htqt-tin-tuc-su-kien');
