'use strict';

/**
 * htqt-tin-tuc-su-kien service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-tin-tuc-su-kien.htqt-tin-tuc-su-kien');
