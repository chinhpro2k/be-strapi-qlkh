'use strict';

/**
 * htqt-trang-chu router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-trang-chu.htqt-trang-chu');
