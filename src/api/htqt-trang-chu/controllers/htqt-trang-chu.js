'use strict';

/**
 * htqt-trang-chu controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-trang-chu.htqt-trang-chu');
