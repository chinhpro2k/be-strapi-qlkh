'use strict';

/**
 * htqt-trang-chu service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-trang-chu.htqt-trang-chu');
