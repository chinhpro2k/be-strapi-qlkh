'use strict';

/**
 * htqt-chuong-trinh-du-an router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::htqt-chuong-trinh-du-an.htqt-chuong-trinh-du-an');
