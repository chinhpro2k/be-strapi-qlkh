'use strict';

/**
 * htqt-chuong-trinh-du-an controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::htqt-chuong-trinh-du-an.htqt-chuong-trinh-du-an');
