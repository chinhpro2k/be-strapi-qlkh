'use strict';

/**
 * htqt-chuong-trinh-du-an service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::htqt-chuong-trinh-du-an.htqt-chuong-trinh-du-an');
