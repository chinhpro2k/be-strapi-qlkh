'use strict';

/**
 * hoat-dong-khoa-hoc router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::hoat-dong-khoa-hoc.hoat-dong-khoa-hoc');
