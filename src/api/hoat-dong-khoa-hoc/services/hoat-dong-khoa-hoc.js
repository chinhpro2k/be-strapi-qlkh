'use strict';

/**
 * hoat-dong-khoa-hoc service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::hoat-dong-khoa-hoc.hoat-dong-khoa-hoc');
