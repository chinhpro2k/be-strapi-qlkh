'use strict';

/**
 * hoat-dong-khoa-hoc controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::hoat-dong-khoa-hoc.hoat-dong-khoa-hoc');
