'use strict';

/**
 * don-vi-nghien-cuu controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::don-vi-nghien-cuu.don-vi-nghien-cuu');
