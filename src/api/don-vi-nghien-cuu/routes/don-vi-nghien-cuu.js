'use strict';

/**
 * don-vi-nghien-cuu router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::don-vi-nghien-cuu.don-vi-nghien-cuu');
