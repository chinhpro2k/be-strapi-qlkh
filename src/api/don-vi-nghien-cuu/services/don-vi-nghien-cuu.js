'use strict';

/**
 * don-vi-nghien-cuu service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::don-vi-nghien-cuu.don-vi-nghien-cuu');

