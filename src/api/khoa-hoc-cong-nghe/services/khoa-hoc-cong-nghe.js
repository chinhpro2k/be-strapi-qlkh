'use strict';

/**
 * khoa-hoc-cong-nghe service
 */

module.exports = () => ({
  articleApiCustom: async (ctx) => {
    try {
      const { query } = ctx;
      console.log('querry', query)
      
      const [entries, count] = await strapi.db.query('api::hoat-dong-khoa-hoc.hoat-dong-khoa-hoc').findWithCount({
        // select: ['title', 'description'],
        where: {
          capDo: query.capDo ? { $eq: query.capDo } : {},
          kieu: query.kieu ? { $eq: query.kieu } : {},
          tieuDe: query.tieuDeStr ? { $containsi: query.tieuDeStr } : {},
          publishedAt:{$notNull:true},
     
        },
        orderBy: [ { publishedAt: 'dsc' }],
        populate: ["deep"],
        offset: query?.page ? (+query?.limit*(+query?.page-1)) : 1,
        limit: query?.limit ? +query?.limit : 3
      });
      console.log('entries=====', entries);
      const dataReturn = entries?.map((item) => {
        const items = { ...item }
        delete items.hinhAnh
        return {

          ...items,
          imageUrl: item.hinhAnh?.formats?.thumbnail?.url


        }
      })

      return {
        data: dataReturn,
        metadata: {
          page: query?.page ?? 1,
          limit: query?.limit ?? 3,
          total: count
        }
      }
    } catch (err) {
      return err
    }
  },

  getByIdService: async (ctx) => {
    try {
      const { id } = ctx.params;
      const { query } = ctx;
      const entries = await strapi.service("api::hoat-dong-khoa-hoc.hoat-dong-khoa-hoc").findOne(id, {
        ...query,
        //   fields: ["slug", "title", "description"],
        populate: ["deep"],
        // filters: {
        //   kieu:ctx.query.type? { $eq: ctx.query.type }:{}
        // }
      })


      return { ...entries, hinhAnh: entries?.hinhAnh?.url }
    } catch (err) {
      return err
    }
  }
});
