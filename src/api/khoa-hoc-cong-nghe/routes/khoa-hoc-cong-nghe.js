module.exports = {
  routes: [
    {
     method: 'GET',
     path: '/khoa-hoc-cong-nghe/all',
     handler: 'khoa-hoc-cong-nghe.getAllData',
     config: {
       policies: [],
       middlewares: [],
     },
    },
    {
      method: 'GET',
      path: '/khoa-hoc-cong-nghe/:id',
      handler: 'khoa-hoc-cong-nghe.getById',
      config: {
        policies: [],
        middlewares: [],
      },
     },
  ],
};
