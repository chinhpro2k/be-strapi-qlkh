'use strict';

/**
 * A set of functions called "actions" for `khoa-hoc-cong-nghe`
 */

module.exports = {
  getAllData: async (ctx, next) => {
    try {
    
      const data = await strapi
        .service("api::khoa-hoc-cong-nghe.khoa-hoc-cong-nghe")
        .articleApiCustom(ctx);
      ctx.body = data;
    } catch (err) {
      ctx.badRequest("controller error", { moreDetails: err });
    }
  },
  getById: async (ctx, next) => {
    try {
    
      const data = await strapi
        .service("api::khoa-hoc-cong-nghe.khoa-hoc-cong-nghe")
        .getByIdService(ctx);
      ctx.body = data;
    } catch (err) {
      ctx.badRequest("controller error", { moreDetails: err });
    }
  }
};

