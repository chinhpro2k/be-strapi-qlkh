'use strict';

/**
 * tap-chi-khoa-hoc controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::tap-chi-khoa-hoc.tap-chi-khoa-hoc');
