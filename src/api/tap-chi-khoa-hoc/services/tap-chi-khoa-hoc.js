'use strict';

/**
 * tap-chi-khoa-hoc service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::tap-chi-khoa-hoc.tap-chi-khoa-hoc');
