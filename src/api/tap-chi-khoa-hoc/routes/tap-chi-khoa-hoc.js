'use strict';

/**
 * tap-chi-khoa-hoc router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::tap-chi-khoa-hoc.tap-chi-khoa-hoc');
